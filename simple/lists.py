#!/usr/bin/python
x = [5, 6.2, "karl"]

for i in x:
	print i

x.append("guenther")
print x[3]

x.insert(1, "eva")
print x[1]

x.pop(1)
print x[1]

print "list len:" + str(len(x))

if 6.2 in x:
	print "yay"
print "==="
print x
print x[0:len(x):2]

x = (4, 5, 6)
