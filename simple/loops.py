#! /usr/bin/python

x = 0

# a while loop ...
print("=== a while loop ===")
while (True):
	x += 1

	if (x > 5):
		break;

	print x

x = [8, 2, 3] # a list!

print("=== a for loop ===")

# a for loop
for i in x:
	print i

print("=== a ranged for loop ===")

for i in range(10):
	print i

print("=== another ranged for loop ===")

for i in range(10, 30, 2):
	print i

print("== yet= another ranged for loop ===")

for i in range(10,30,2):
	print i

print("=== a last loop ===")

for i in range (10):
	if not i % 3:
		print i	
